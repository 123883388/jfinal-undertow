# jfinal-undertow

#### 项目介绍
jfinal-undertow 用于开发、部署由 jfinal 开发的 web 项目。独创 HotSwapClassLoader + HotSwapWatcher 以 321 行代码极简实现热加载开发与部署，前无古人，后必有模仿者



## 一、 极速上手
### 1: 添加 maven 依赖
```
<dependency>
	<groupId>com.jfinal</groupId>
	<artifactId>jfinal-undertow</artifactId>
	<version>1.9</version>
</dependency>
```

注意：以前对 jetty-server 的 maven 依赖要删掉

### 2: 创建 main 方法在 eclipse 或 IDEA 中启动项目
```
UndertowServer.start(AppConfig.class);
```

其中 AppConfig 是继承自 JFinalConfig 的子类，以前的 JFinal.start(...) 用法不再需要



## 二、极速打包与部署
### 1: 修改 pom.xml 头部的打包类型由 war 改成 jar
```
<packaging>jar</packaging> 
```

### 2: 在 pom.xml 中添加 maven-jar-plugin 插件
```
<!--
    jar 包中的配置文件优先级高于 config 目录下的 "同名文件"
    因此，打包时需要排除掉 jar 包中来自 src/main/resources 目录的
    配置文件，否则部署时 config 目录中的同名配置文件不会生效
-->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <version>2.6</version>
    <configuration>
        <excludes>
            <exclude>*.txt</exclude>
            <exclude>*.xml</exclude>
            <exclude>*.properties</exclude>
        </excludes>
    </configuration>
</plugin>
```
该插件仅为了避免将配置文件打入 jar 包，如果是打成 fatjar 包则不需要添加此插件

### 3: 在 pom.xml 中添加 maven-assembly-plugin 插件
```
<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-assembly-plugin</artifactId>
	<version>3.1.0</version>
	<executions>
		<execution>
			<id>make-assembly</id>
			<phase>package</phase>
			<goals>
				<goal>single</goal>
			</goals>

			<configuration>
				<!-- 打包生成的文件名 -->
				<finalName>${project.artifactId}</finalName>
				<!-- jar 等压缩文件在被打包进入 zip、tar.gz 时是否压缩，设置为 false 可加快打包速度 -->
				<recompressZippedFiles>false</recompressZippedFiles>
				<!-- 打包生成的文件是否要追加 release.xml 中定义的 id 值 -->
				<appendAssemblyId>true</appendAssemblyId>
				<!-- 指向打包描述文件 package.xml -->
				<descriptors>
					<descriptor>package.xml</descriptor>
				</descriptors>
				<!-- 打包结果输出的基础目录 -->
				<outputDirectory>${project.build.directory}/</outputDirectory>
			</configuration>
		</execution>
	</executions>
</plugin>
```

### 4、在项目根目录下添加打包描述文件 package.xml，内容如下：
```
<assembly xmlns="http://maven.apache.org/ASSEMBLY/2.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/ASSEMBLY/2.0.0 http://maven.apache.org/xsd/assembly-2.0.0.xsd">
	<!--
	assembly 打包配置更多配置可参考官司方文档：
	http://maven.apache.org/plugins/maven-assembly-plugin/assembly.html
	-->

	<id>release</id>

	<!--
		设置打包格式，可同时设置多种格式，常用格式有：dir、zip、tar、tar.gz
		dir 格式便于在本地测试打包结果
		zip 格式便于 windows 系统下解压运行
		tar、tar.gz 格式便于 linux 系统下解压运行
	-->
	<formats>
		<format>dir</format>
		<format>zip</format>
		<!-- <format>tar.gz</format> -->
	</formats>

	<!-- 打 zip 设置为 true 会在包在存在总目录，打 dir 时设置为 false 少层目录 -->
	<includeBaseDirectory>true</includeBaseDirectory>

	<fileSets>
		<!-- src/main/resources 全部 copy 到 config 目录下 -->
		<fileSet>
			<directory>${basedir}/src/main/resources</directory>
			<outputDirectory>config</outputDirectory>
		</fileSet>

		<!-- src/main/webapp 全部 copy 到 webapp 目录下 -->
		<fileSet>
			<directory>${basedir}/src/main/webapp</directory>
			<outputDirectory>webapp</outputDirectory>
		</fileSet>

		<!-- 项目根下面的脚本文件 copy 到根目录下 -->
		<fileSet>
			<directory>${basedir}</directory>
			<outputDirectory></outputDirectory>
			<!-- 脚本文件在 linux 下的权限设为 755，无需 chmod 可直接运行 -->
			<fileMode>755</fileMode>
			<includes>
				<include>*.sh</include>
				<include>*.bat</include>
			</includes>
		</fileSet>
	</fileSets>

	<!-- 依赖的 jar 包 copy 到 lib 目录下 -->
	<dependencySets>
		<dependencySet>
			<outputDirectory>lib</outputDirectory>
		</dependencySet>
	</dependencySets>
</assembly>
```

### 5、部署在项目根目录下添加项目运行脚本文件
本项目中的根目录下面提供了 jfinal.sh、jfinal.bat 这两个脚本文件下载。其中 jfinal.sh 用于 linux、mac 系统，jfinal.bat 用于 windows 系统，注意要修改一下这些脚本文件中的 MAIN_CLASS 变量指向你自己的项目入口，例如：
```
MAIN_CLASS=com.yourpackage.YourMainClass
```
当然这两个脚本文件不是必须的，大家完全可以根据个人习惯编写启动脚本

### 6、在命令行运行打包指令
```
mvn clean package
```

### 7、部署
   进入项目的 target/your-project-release 目录，运行 ./jfinal.sh start 即可启动项目。target 目录下还会打包出一个 your-project-release.zip 该文件是上述第五步中生成的目录的一个 zip 压缩文件，上传该文件到服务器解压即部署，可以通过修改 package.xml 改变生成的文件名或者取消生成该文件

## 三、jfinal-undertow 优势：
1：极速启动，启动速度比 tomcat 快 5 到 8 倍。jfinal.com 官网启动时间在 1.5 秒内

2：极简精妙的热部署设计，实现极速轻量级热部署，响应极为迅速，让开发体验再次提升一个档次

3：性能比 tomcat、jetty 高出很多，可代替 tomcat、jetty 用于生产环境

4：undertow 为嵌入式而生，可直接用于生产环境部署，部署时无需下载服务，无需配置服务，极其适合微服务开发、部署

5：告别 web.xml、告别 tomcat、告别 jetty，节省大量打包与部署时间。令开发、打包、部署成为一件开心的事

6：功能丰富，支持 classHotSwap、WebSocket、gzip 压缩、servlet、filter、sessionHotSwap 等功能

7：支持 fatjar 与 非 fatjar 打包模式，为 jfinal 下一步的微服务功能做好准备

8：开发、打包、部署一体化，整个过程无需对项目中的任何地方进行调整或修改，真正实现从极速开发到极速部署

9：以上仅为 jfinal-undertow 的部分功能，更多好用的功能如 fatjar 打包模式见 jfinal 官网文档

